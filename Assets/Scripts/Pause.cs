﻿using UnityEngine;
using System.Collections;

public class Pause : MonoBehaviour {

	public bool pauseBtn;

	// Use this for initialization
	void Start () {
		pauseBtn = false;
	}
	
	// Update is called once per frame
	void Update () {

	}
	public void pauseBtnPressed()
	{
		pauseBtn = !pauseBtn; 

		if(pauseBtn == true)
		{
			Time.timeScale = 0;
		}
		else {
			Time.timeScale =  1;
		}
	}

	public void menuPressed() {
		Application.LoadLevel (0);
	}
}
