﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class MenuScript : MonoBehaviour {
	
	public Canvas quitMenu, helpMenu;
	public Button playText;
	public Button exitText, helpText;

	// Use this for initialization
	void Start () {
		quitMenu = quitMenu.GetComponent<Canvas> ();
		playText = playText.GetComponent<Button> ();
		exitText = exitText.GetComponent<Button> ();
		helpText = helpText.GetComponent<Button> ();
		
		quitMenu.enabled = false;
		helpMenu.enabled = false;
	}

	public void helpPress() {
		
		quitMenu.enabled = false;
		helpMenu.enabled = true;
		playText.enabled = false;
		exitText.enabled = false;
	}

	public void exitPress() {
		quitMenu.enabled = true;
		helpMenu.enabled = false;
		playText.enabled = false;
		exitText.enabled = false;
	}
	
	public void noPress() {
		quitMenu.enabled = false;
		helpMenu.enabled = false;
		playText.enabled = true;
		exitText.enabled = true;
	}

	public void backPressed() {
		quitMenu.enabled = false;
		helpMenu.enabled = false;
		playText.enabled = true;
		exitText.enabled = true;
	}


	
	public void startLevel() {
		Application.LoadLevel(1);
	}
	
	public void exitGame() {
		Application.Quit();
        
	}
	

	
}
