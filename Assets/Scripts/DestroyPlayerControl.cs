﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class DestroyPlayerControl : MonoBehaviour {

	public Text gameOverText, startMenu, retryText;

	public Button goToStartMenu;

	public AudioClip deadUpSound;
	AudioSource deadUpSource;

	// Use this for initialization
	void Start () {
		gameOverText.text = "";
		startMenu.text = "";
		retryText.text = "";

		goToStartMenu = goToStartMenu.GetComponent<Button> ();
		goToStartMenu.enabled = false;

		deadUpSource = GetComponent<AudioSource>();



	}
	

	
	void OnTriggerEnter(Collider other) {
        if (other.gameObject.CompareTag ("Player")) {
			other.gameObject.SetActive(false);



			gameOverText.text = "Game Over";
			startMenu.text = "Start Menu";
			retryText.text = "Retry";

			goToStartMenu.enabled = true;

			deadUpSource.clip = deadUpSound;
			deadUpSource.Play();

			Time.timeScale = 0;
		}



	}

	public void startMenuPressed() {
		Application.LoadLevel (0);
	}


	public void retryBtnPressed()
	{
		if (Application.loadedLevel == 1)
			Application.LoadLevel (1);
		if (Application.loadedLevel == 2)
			Application.LoadLevel (2);
		
		if (Application.loadedLevel == 3)
			Application.LoadLevel (3);
		if (Application.loadedLevel == 4)
			Application.LoadLevel (4);
		if (Application.loadedLevel == 5)
			Application.LoadLevel (5);
	}

}
