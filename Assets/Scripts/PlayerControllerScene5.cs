﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PlayerControllerScene5 : MonoBehaviour {
	

	public float speed;
	private Rigidbody rb;
	private int count;
	public Text countText;
	public Canvas winCanvas;

	float accly,acclx;

	public AudioClip pickUpSound;
	AudioSource pickUpSource;
	
	void Start() {
		rb = GetComponent<Rigidbody>();
		count = 0;
		setCountText();
		Time.timeScale = 1;
		winCanvas = winCanvas.GetComponent<Canvas>();
		winCanvas.enabled = false;

		acclx = Input.acceleration.x;
		accly = Input.acceleration.y;

		pickUpSource = GetComponent<AudioSource>();
	}
	
	

	
	void FixedUpdate()
	{
		
		if (SystemInfo.deviceType == DeviceType.Desktop) {
			float moveHorizontal = Input.GetAxis("Horizontal");
			float moveVertical = Input.GetAxis("Vertical");
			
			Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical );
			
			rb.AddForce(movement * speed);
			
		} else {
			Vector3 movement = Vector3.zero;
			
			float moveHorizontal = (Input.acceleration.x -acclx)*speed;
			float moveVertical = (Input.acceleration.y - accly)*speed;
			
			if (movement.sqrMagnitude > 1)
				movement.Normalize();
			
			movement = new Vector3(moveHorizontal*speed, 0.0f, moveVertical*speed );
			
			rb.AddForce (movement*speed*Time.deltaTime);
		}
	}
	
	void OnTriggerEnter(Collider other) {
		if (other.gameObject.CompareTag ("Pick Up")) {
			other.gameObject.SetActive(false);

			pickUpSource.clip = pickUpSound;
			pickUpSource.Play();

			count++;
			setCountText();
		}
		
	}
	
	void setCountText() {
		countText.text = "Count: " + count.ToString();
		if (count >= 10) {
			Time.timeScale = 0;
			winCanvas.enabled = true;
		
		}
	}

	public void starMenuPressed() {
		Application.LoadLevel(0);
	}
}
