﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Timer : MonoBehaviour {
	float countDown;
	public Text countDownText;



	public Text gameOverText, startMenu, retryText;
	
	public Button goToStartMenu;
	
	public AudioClip deadUpSound;
	AudioSource deadUpSource;



	// Use this for initialization
	void Start () {
		countDownText.text = "";
		gameOverText.text = "";
		startMenu.text = "";
		retryText.text = "";
		
		goToStartMenu = goToStartMenu.GetComponent<Button> ();
		goToStartMenu.enabled = false;
		
		deadUpSource = GetComponent<AudioSource>();

		if (Application.loadedLevel == 1)
			countDown = 20;
		if (Application.loadedLevel == 2)
			countDown = 30;
		if (Application.loadedLevel == 3)
			countDown = 40;
		if (Application.loadedLevel == 4)
			countDown = 150;
		if (Application.loadedLevel == 5)
			countDown = 200;

	}
	
	void Update() {
		if (countDown >= 0) {
			countDown -= Time.deltaTime;
			
			countDownText.text = "Time: " + (int)countDown;
		} else {
			gameOver();
		}
	}

	void gameOver() {
		gameOverText.text = "Game Over";
		startMenu.text = "Start Menu";
		retryText.text = "Retry";
		
		goToStartMenu.enabled = true;
		
		deadUpSource.clip = deadUpSound;
		deadUpSource.Play();
		
		Time.timeScale = 0;
	}
}
